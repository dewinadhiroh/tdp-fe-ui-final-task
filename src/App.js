import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='top-nav'>
        <div className='nav-button'>
          <a href="/#"><b>EDTS</b> TDP Batch #2</a>
          <div className='nav-button-right'>
            <a href="/#contact">Contact</a>
            <a href="/#about">About</a>
            <a href="/#projects">Projects</a> 
          </div>
        </div>
      </div>


        {/* Header */}
        <header className='header-image'>
          <img src={banner} alt="Architecture" width={1500} height={800} />
            <div className='header-text'>
              <h1>
                <span className='edts'>EDTS</span>
                <span className='architects'>Architects</span> 
              </h1>
            </div>
        </header>

        {/* Page content */}
          {/* Project Section */}
          <div className='main-container'>
          <div id="projects" className ='container-project'>
            <div className='project-title'>
              <h3 className='project-name'>Projects</h3>
              <hr></hr>
            </div>

            <div className='project-row-padding'>
              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Summer House</div>
                  <img src={house2} alt="House"/>
                </div>
              </div>
          
              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Brick House</div>
                  <img src={house3} alt="House"/>
                </div>
              </div>

              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Renovated</div>
                  <img src={house4} alt="House"/>
                </div>
              </div>

              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Barn House</div>
                  <img src={house5} alt="House"/>
                </div>
              </div>
            </div>

            <div className='project-row-padding'>
              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Summer House</div>
                  <img src={house3} alt="House"/>
                </div>
              </div>
          
              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Brick House</div>
                  <img src={house2} alt="House"/>
                </div>
              </div>

              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Renovated</div>
                  <img src={house5} alt="House"/>
                </div>
              </div>

              <div className='project-column-padding'>
                <div className='content-container'>
                  <div className='name-house'>Barn House</div>
                  <img src={house4} alt="House"/>
                </div>
              </div>
            </div>
          </div>

          {/* About Section */}
          <div id="about">
            <div className='project-title'>
              <h3 className='project-name'>About</h3>
              <hr></hr>
            </div>
              <p className='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Excepteur sint
                occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
                adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat.
              </p>

            <div className='about-row-container'>
              <div className='about-column-container'>
                <img src={team1} alt="Jane"/>
                <h3>Jane Doe</h3>
                <p className='level'>CEO &amp; Founder</p>
                <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                <p><button>Contact</button></p>
              </div>

              <div className='about-column-container'>
                <img src={team2} alt="John"/>
                <h3>John Doe</h3>
                <p className='level'>Architect</p>
                <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                <p><button>Contact</button></p>
              </div>

              <div className='about-column-container'>
                <img src={team3} alt="Mike"/>
                <h3>Mike Ross</h3>
                <p className='level'>Architect</p>
                <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                <p><button>Contact</button></p>
              </div>

              <div className='about-column-container'>
                <img src={team4} alt="Dan"/>
                <h3>Dan Star</h3>
                <p className='level'>Architect</p>
                <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                <p><button>Contact</button></p>
              </div>
            </div>

          </div>
          {/* Contact Section */}
          <div className='container-contact' id="contact">
            <h3 className='project-name'>Contact</h3>
            <hr></hr>
            <p>Lets get in touch and talk about your next project.</p>
            <form action="/action_page.php">
              <input id='name' type={'text'} name='name' placeholder='Name' required></input>
              <input id='email'type={'email'} name='email' placeholder='Email'required></input>
              <input id='subject' type={'text'} name='subject' placeholder='Subject'required></input>
              <input id='comment' type={'text'} name='comment' placeholder='Comment'required></input>
              <input id='submit'  type={'submit'} value='SEND MESSAGE'></input>
            </form>
          </div>

          {/* Image of location/map */}
          <div>
            <img src={map} alt="maps" />
          </div>

          <div id='contact' className='contact-container'>
            <h3>Contact List</h3>
            <hr></hr>
            <table>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Country</th>
              </tr>
              <tr>
                <td>Alfreds Futterkiste</td>
                <td>alfreds@gmail.com</td>
                <td>Germany</td>
              </tr>
              <tr>
                <td>Reza</td>
                <td>reza@gmail.com</td>
                <td>Indonesia</td>
              </tr>
              <tr>
                <td>Ismail</td>
                <td>ismail@gmail.com</td>
                <td>Turkey</td>
              </tr>
              <tr>
                <td>Holand</td>
                <td>holand@gmail.com</td>
                <td>Belanda</td>
              </tr>
            </table>
          </div>

          {/* End page content */}
      </div>       

        {/* Footer */}
        <footer>
          <p className='text-footer'>Copyright 2022</p>
        </footer>

    </div>
  );
}

export default App;
